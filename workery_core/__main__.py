if __name__ == '__main__':
    from faker import Faker
    from os import getenv
    from psycopg import connect
    from psycopg.errors import UniqueViolation
    from rethinkdb import RethinkDB
    from workery_core.connectors.rethinkdb_queries import (
        RethinkDBQuery
    )
    from workery_core.connectors.sql_queries import (
        SQLQuery
    )
    from workery_core.controllers.addresses import (
        Address
    )
    from workery_core.controllers.auths import (
        Auth
    )
    from workery_core.controllers.keys import (
        Key
    )
    from workery_core.controllers.persons import (
        Person
    )
    from workery_core.controllers.roles import (
        Role
    )
    from workery_core.controllers.secrets import (
        Secret
    )
    from workery_core.exceptions import (
        SecurityError,
        ValueExists
    )
    from workery_core.utils import randomize


    faker = Faker(
        locale='pt_br'
    )
    connection = connect(
        getenv(
            'WORKERY_SQL'
        )
    )
    email = 'eumesmo@exemplo.com'
    phone = '+5513992031899'
    password = '123456'
    try:
        Person.auth.new(
            email=email,
            phone=phone,
            password=password
        )
    except ValueExists:
        pass
    me = Person(
        identifier=Person.auth.login(
            value=phone,
            password=password
        )
    )
    me.address.new(
        location='Rua das casas, sn',
        district='Favela do caixão',
        city='Piripiri',
        state='Líquido',
        country='Atlântida'
    )
