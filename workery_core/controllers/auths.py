from argon2 import PasswordHasher
from argon2.exceptions import (
    VerifyMismatchError
)
from datetime import (
    datetime,
    UTC
)
from email_validator import (
    validate_email,
    EmailNotValidError
)
from psycopg.errors import (
    UniqueViolation
)
from typing import (
    LiteralString,
    NoReturn,
    Self,
    Union
)
from uuid import (
    uuid4,
    UUID
)
from workery_core.connectors.sql_queries import (
    SQLQuery
)
from workery_core.exceptions import (
    PhoneNumberError,
    SecurityError,
    ValueExists,
    WrongPasswordError
)


class Auth:

    __hasher = PasswordHasher()

    def __init__(
        self: Self,
        identifier: UUID
    ) -> NoReturn:
        self.identifier = identifier
        values = SQLQuery.show(
            """
                SELECT
                    auths.phone,
                    auths.email,
                    auths.google,
                    auths.apple,
                    auths.create_in,
                    auths.enable_in
                FROM
                    auths
                WHERE
                    auths.id = %s
                LIMIT
                    1 ;
            """,
            (self.identifier, )
        )
        self.phone = values[0][0]
        self.email = values[0][1]
        self.google = values[0][2]
        self.apple = values[0][3]
        self.create_in = values[0][4]
        self.enable_in = values[0][5]
        self.roles = [
            field[0] for field in SQLQuery.show(
                """
                    SELECT
                        roles.name
                    FROM
                        auths
                    INNER JOIN
                        auth_roles
                    ON
                        auths.id = %s
                    INNER JOIN
                        roles
                    ON
                        roles.id = auth_roles.role ;
                """,
                (self.identifier, )
            )
        ]

    def __check_phone(
        self: Self,
        value: str
    ) -> str:
        value = value.replace(
            'tel:', ''
        ).replace(
            '-', ''
        )
        if len(
            value
        ) >= 14 and len(
            value
        ) <= 15 and value[
            0
        ] == '+':
            return value
        else:
            raise PhoneNumberError(
                'Phone number must be formatted as '
                f'\'+1234567890123\', not { value }'
            )

    def __check_password(
        self: Self,
        value: str
    ) -> str:
        value = value
        if len(value) >= 6:
            return value
        else:
            raise WrongPasswordError(
                'Password is less than 6 digits.'
            )

    @classmethod
    def new(
        cls: Self,
        phone: LiteralString,
        email: LiteralString,
        password: str
    ) -> bool:
        try:
            SQLQuery.execute(
                """
                    INSERT INTO
                        auths
                            (
                                phone,
                                email,
                                password,
                                create_in
                            )
                    VALUES
                        (
                            %s, %s, %s, %s
                        ) ;
                """,
                (
                    cls.__check_phone(
                        cls, phone
                    ),
                    validate_email(
                        email,
                        check_deliverability=False
                    ).normalized,
                    cls.__hasher.hash(
                        cls.__check_password(
                            cls, password
                        )
                    ),
                    datetime.now(UTC)
                )
            )
        except UniqueViolation:
            person_exists = SQLQuery.show(
                """
                    SELECT
                        id
                    FROM
                        auths
                    WHERE
                        phone = %s
                    OR
                        email = %s
                    LIMIT
                        1
                """,
                (phone, email, )
            )
            if len(person_exists) > 0:
                raise ValueExists(
                    'Phone and or email already exists.'
                )
        else:
            return True

    @classmethod
    def login(
        cls: Self,
        value: LiteralString,
        password: str
    ) -> Union[str, None]:
        """
            It must return the user id. In case of error, return None
        """
        auth = SQLQuery.show(
            """
                SELECT
                    id,
                    password
                FROM
                    auths
                WHERE
                    phone = %s
                OR
                    email = %s
                LIMIT
                    1 ;
            """,
            (value, value)
        )
        if len(auth) == 1:
            try:
                cls.__hasher.verify(
                    auth[0][1], password
                )
            except VerifyMismatchError:
                raise SecurityError(
                    'Wrong password'
                )
            else:
                return str(auth[0][0])
        else:
            raise SecurityError(
                'Person not found'
            )

    def add_google(
        self: Self,
        value: LiteralString
    ) -> bool:
        show_value = SQLQuery.show(
            """
                SELECT
                    google
                FROM
                    auths
                WHERE
                    id = %s
                LIMIT
                    1 ;
            """,
            (self.identifier, )
        )[0][0]
        if show_value == None:
            SQLQuery.execute(
                """
                    UPDATE
                        auths
                    SET
                        google = %s
                    WHERE
                        id = %s
                """,
                (value, self.identifier, )
            )
            return True
        else:
            return False

    def add_apple(
        self: Self,
        value: LiteralString
    ) -> bool:
        show_value = SQLQuery.show(
            """
                SELECT
                    apple
                FROM
                    auths
                WHERE
                    id = %s
            """,
            (self.identifier, )
        )[0][0]
        if show_value == None:
            SQLQuery.execute(
                """
                    UPDATE
                        auths
                    SET
                        apple = %s
                    WHERE
                        id = %s
                """,
                (value, self.identifier, )
            )
            return True
        else:
            return False

    def change_email(
        self: Self,
        new_email: LiteralString
    ) -> bool:
        SQLQuery.execute(
            """
                UPDATE
                    auths
                SET
                    email = %s
                WHERE
                    id = %s
            """,
            (
                validate_email(
                    new_email,
                    check_deliverability=False
                ).normalized,
                self.identifier
            )
        )
        self.email = new_email
        return True

    def change_phone(
        self: Self,
        new_phone: LiteralString
    ) -> bool:
        SQLQuery.execute(
            """
                UPDATE
                    auths
                SET
                    phone = %s
                WHERE
                    id = %s
            """,
            (new_phone, self.identifier)
        )
        self.phone = new_phone
        return True

    def change_password(
        self: Self,
        new_password: str
    ) -> bool:
        SQLQuery.execute(
            """
                UPDATE
                    auths
                SET
                    password = %s
                WHERE
                    id = %s
            """,
            (
                self.__hasher.hash(
                    new_password
                ),
                self.identifier
            )
        )
        return True

    @property
    def remove(
        self: Self
    ) -> bool:
        SQLQuery.execute(
            """
                DELETE FROM
                    auths
                WHERE
                    id = %s
            """,
            (self.identifier, )
        )
        return True
