from typing import (
    NoReturn,
    Self
)
from uuid import UUID
from workery_core.controllers.addresses import (
    Address
)
from workery_core.controllers.auths import (
    Auth
)
from workery_core.controllers.keys import (
    Key
)


class Person:
    auth: Auth = Auth
    address: Address = Address

    def __init__(
        self: Self,
        identifier: UUID
    ) -> NoReturn:
        self.auth = Auth(
            identifier
        )
        self.address = Address(
            identifier
        )
        self.key = Key
