from typing import (
    NoReturn,
    Self,
    Union
)
from uuid import uuid4, UUID
from workery_core.connectors.sql_queries import (
    SQLQuery
)


class Role:

    @classmethod
    def new(
        cls: Self,
        name: str,
        description: str
    ) -> bool:
        SQLQuery.execute(
            """
                INSERT INTO roles (name, description)
                VALUES (%s, %s) ;
            """,
            (name, description, )

        )
        return True

    @classmethod
    @property
    def show(
        cls: Self
    ) -> list[dict[str, Union[UUID, str]]]:
        return [
            {
                'id': row[0],
                'name': row[1],
                'description': row[2]
            } for row in SQLQuery.show(
                """
                    SELECT * FROM roles ;
                """,
                ()
            )
        ]

    @classmethod
    def rename(
        cls: Self,
        role: Union[UUID, str],
        new_name: str
    ) -> bool:
        SQLQuery.execute(
            """
                UPDATE roles
                SET name = %s
                WHERE id = %s
            """,
            (
                new_name,
                role,
            )
        )
        return True

    @classmethod
    def change_description(
        cls: Self,
        role: Union[UUID, str],
        new_description: str
    ) -> bool:
        SQLQuery.execute(
            """
                UPDATE roles
                SET description = %s
                WHERE id = %s
            """,
            (
                new_description,
                role,
            )
        )
        return True

    @classmethod
    def remove(
        cls: Self,
        role: Union[UUID, str]
    ) -> bool:
        SQLQuery.execute(
            """
                DELETE FROM roles
                WHERE id = %s ;
            """,
            (role, )
        )
        return True

    @classmethod
    def add_person(
        cls: Self,
        person: Union[UUID, str],
        role: Union[UUID, str]
    ) -> bool:
        SQLQuery.execute(
            """
                INSERT INTO auth_roles (
                    auth, role
                )
                VALUES (
                    %s, %s
                ) ;
            """,
            (
                person, role,
            )
        )
        return True

    @classmethod
    def remove_person(
        cls: Self,
        person: Union[UUID, str],
        role: Union[UUID, str]
    ) -> bool:
        SQLQuery.execute(
            """
                DELETE FROM auth_roles
                WHERE auth = %s
                AND role = %s ;
            """,
            (
                person, role,
            )
        )
        return True
