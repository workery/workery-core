from os import getenv
from psycopg import (
    connect
)
from typing import (
    NoReturn,
    Self
)


class SQLQuery:

    @classmethod
    def show(
        self: Self,
        text: str,
        values: tuple[str]
    ) -> tuple[str]:
        """
            Returns the requested data to the database
        """
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor() as cursor:
                return cursor.execute(
                    text, values
                ).fetchall()

    @classmethod
    def execute(
        self: Self,
        text: str,
        values: tuple[str]
    ) -> NoReturn:
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    text, values
                )
                connection.commit()
