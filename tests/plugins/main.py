from faker import Faker
from pytest import fixture
from workery_core.controllers.addresses import (
    Address
)


@fixture(scope='module')
def faker() -> Faker:
    return Faker(
        locale='pt-br'
    )
