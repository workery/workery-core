from faker import Faker
from pytest import fixture


@fixture(scope='module')
def default_role(
    faker: Faker
):
    return {
        'name': faker.cryptocurrency_name()[:15],
        'description': faker.paragraph()
    }

@fixture(scope='module')
def other_role(
    faker: Faker
):
    return {
        'name': faker.cryptocurrency_name()[:15],
        'description': faker.paragraph()
    }
