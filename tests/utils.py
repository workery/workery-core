def format_phone(number: str) -> str:
    return number.replace(
        '(', ''
    ).replace(
        ')', ''
    ).replace(
        '-', ''
    ).replace(
        ' ', ''
    )
