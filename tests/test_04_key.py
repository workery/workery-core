from pytest import raises
from typing import (
    NoReturn,
    Self
)
from workery_core.controllers.keys import (
    Key
)
from workery_core.exceptions import (
    SecurityError
)


class TestKey:
    def test_create_new_key(
        self: Self
    ) -> NoReturn:
        assert type(
            Key.new(
                value='any data'
            )
        ) == str

    def test_recover_key(
        self: Self
    ) -> NoReturn:
        person_id = '1234'
        key = Key.new(
            value=person_id
        )
        assert type(
            Key.recover(
                key=key,
                value=person_id
            )
        ) == str

    def test_raise_wrong_key(
        self: Self
    ) -> NoReturn:
        with raises(SecurityError):
            Key.recover(
                key='wrong_key',
                value='any value'
            )

    def test_raise_wrong_value(
        self: Self
    ) -> NoReturn:
        key = Key.new(
            value='1234'
        )
        with raises(SecurityError):
            Key.recover(
                key=key,
                value='other_value'
            )
