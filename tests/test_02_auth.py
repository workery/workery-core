from argon2 import PasswordHasher
from argon2.exceptions import (
    VerifyMismatchError
)
from faker import Faker
from os import getenv
from psycopg import connect
from pytest import raises
from typing import (
    Callable,
    NoReturn,
    Self
)
from uuid import UUID
from workery_core.exceptions import (
    PhoneNumberError,
    SecurityError,
    ValueExists,
    WrongPasswordError
)
from workery_core.controllers.persons import (
    Person
)


class TestAuth:
    __hasher = PasswordHasher()

    def test_create_new_auth(
        self: Self,
        other_person: dict[str]
    ) -> NoReturn:
        assert Person.auth.new(
            phone=other_person[
                'phone'
            ],
            email=other_person[
                'email'
            ],
            password=other_person[
                'password'
            ]
        ) == True
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor(
            ) as cursor:
                a = cursor.execute(
                    """
                        SELECT phone, email
                        FROM auths
                        WHERE phone = %s
                        AND email = %s
                        LIMIT 1 ;
                    """,
                    [
                        other_person[
                            'phone'
                        ],
                        other_person[
                            'email'
                        ]
                    ]
                ).fetchone()
                assert a[0] == other_person[
                    'phone'
                ]
                assert a[1] == other_person[
                    'email'
                ]

    def test_raise_creating_same_person(
        self: Self,
        other_person: dict
    ) -> NoReturn:
        with raises(ValueExists):
            Person.auth.new(
                phone=other_person[
                    'phone'
                ],
                email=other_person[
                    'email'
                ],
                password=other_person[
                    'password'
                ]
            )

    def test_raise_creating_same_phone(
        self: Self,
        other_person: dict,
        anonymous: dict
    ) -> NoReturn:
        with raises(ValueExists):
            Person.auth.new(
                phone=other_person[
                    'phone'
                ],
                email=anonymous[
                    'email'
                ],
                password=other_person[
                    'password'
                ]
            )

    def test_raise_creating_same_email(
        self: Self,
        other_person: dict,
        anonymous: dict
    ) -> NoReturn:
        with raises(ValueExists):
            Person.auth.new(
                phone=anonymous[
                    'phone'
                ],
                email=other_person[
                    'email'
                ],
                password=other_person[
                    'password'
                ]
            )

    def test_instantiate_auth(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        person = Person(
            default_person['id']
        )
        assert person.auth.identifier == default_person[
            'id'
        ]

    def test_raise_creating_with_wrong_phone(
        self: Self,
        anonymous: dict[str]
    ) -> NoReturn:
        with raises(PhoneNumberError):
            Person.auth.new(
                phone='wrong_number',
                email=anonymous[
                    'email'
                ],
                password=anonymous[
                    'password'
                ]
            )

    def test_raise_creating_with_wrong_email(
        self: Self,
        anonymous: dict[str]
    ) -> NoReturn:
        with raises(ValueError):
            Person.auth.new(
                phone=anonymous[
                    'phone'
                ],
                email='wrong_email',
                password=anonymous[
                    'password'
                ]
            )

    def test_raise_password_smaller_than_6_digits(
        self: Self,
        anonymous: dict[str]
    ) -> NoReturn:
        with raises(WrongPasswordError):
            Person.auth.new(
                phone=anonymous[
                    'phone'
                ],
                email=anonymous[
                    'email'
                ],
                password='12345'
            )

    def test_auth_login_with_phone(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        assert Person.auth.login(
            value=default_person[
                'phone'
            ],
            password=default_person[
                'password'
            ]
        ) == default_person['id']

    def test_auth_login_with_email(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        assert Person.auth.login(
            value=default_person[
                'email'
            ],
            password=default_person[
                'password'
            ]
        ) == default_person['id']

    def test_raise_login_with_value_wrong(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        with raises(SecurityError):
            Person.auth.login(
                value='wrong_identifier',
                password=default_person[
                    'password'
                ]
            )

    def test_raise_login_with_password_wrong(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        with raises(SecurityError):
            Person.auth.login(
                value=default_person[
                    'email'
                ],
                password='wrong_password'
            )

    def test_adding_google_account(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        person = Person.auth(
            default_person[
                'id'
            ]
        )
        assert person.add_google(
            default_person[
                'google'
            ]
        ) == True
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor(
            ) as cursor:
                a = cursor.execute(
                    """
                        SELECT google
                        FROM auths
                        WHERE id = %s;
                    """,
                    [
                        default_person[
                            'id'
                        ]
                    ]
                ).fetchone()
                assert a[0] == default_person[
                    'google'
                ]

    def test_raise_adding_google_account_again(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        person = Person.auth(
            default_person[
                'id'
            ]
        )
        assert person.add_google(
            default_person[
                'google'
            ]
        ) == False

    def test_adding_apple_account(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        person = Person.auth(
            default_person[
                'id'
            ]
        )
        assert person.add_apple(
            default_person[
                'apple'
            ]
        ) == True
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor(
            ) as cursor:
                a = cursor.execute(
                    """
                        SELECT apple
                        FROM auths
                        WHERE id = %s;
                    """,
                    [
                        default_person[
                            'id'
                        ]
                    ]
                ).fetchone()
                assert a[0] == default_person[
                    'apple'
                ]

    def test_raise_adding_apple_account_again(
        self: Self,
        default_person: dict[str]
    ) -> NoReturn:
        person = Person.auth(
            default_person[
                'id'
            ]
        )
        assert person.add_apple(
            default_person[
                'apple'
            ]
        ) == False

    def test_changing_email(
        self: Self,
        faker: Faker,
        default_person: dict[str]
    ) -> NoReturn:
        new_email = faker.email()
        person = Person.auth(
            default_person[
                'id'
            ]
        )
        person.change_email(
            new_email=new_email
        )
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor(
            ) as cursor:
                a = cursor.execute(
                    """
                        SELECT email
                        FROM auths
                        WHERE id = %s;
                    """,
                    [
                        default_person[
                            'id'
                        ]
                    ]
                ).fetchone()
                assert a[0] == new_email

    def test_changing_phone(
        self: Self,
        faker: Faker,
        default_person: dict[str],
        anonymous: dict[str]
    ) -> NoReturn:
        new_phone = anonymous['phone']
        person = Person.auth(
            default_person[
                'id'
            ]
        )
        person.change_phone(
            new_phone=new_phone
        )
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor(
            ) as cursor:
                a = cursor.execute(
                    """
                        SELECT phone
                        FROM auths
                        WHERE id = %s;
                    """,
                    [
                        default_person[
                            'id'
                        ]
                    ]
                ).fetchone()
                assert a[0] == new_phone

    def test_changing_password(
        self: Self,
        faker: Faker,
        default_person: dict[str]
    ) -> NoReturn:
        new_password = faker.password()
        person = Person.auth(
            default_person[
                'id'
            ]
        )
        person.change_password(
            new_password=new_password
        )
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor(
            ) as cursor:
                a = cursor.execute(
                    """
                        SELECT password
                        FROM auths
                        WHERE id = %s;
                    """,
                    [
                        default_person[
                            'id'
                        ]
                    ]
                ).fetchone()
                assert self.__hasher.verify(
                    a[0], new_password
                ) == True

    def test_removing_auth(
        self: Self,
        other_person: dict[str]
    ) -> NoReturn:
        person = Person.auth(
            Person.auth.login(
                value=other_person[
                    'email'
                ],
                password=other_person[
                    'password'
                ]
            )
        )
        person.remove
        with connect(
            getenv(
                'WORKERY_SQL'
            )
        ) as connection:
            with connection.cursor(
            ) as cursor:
                assert cursor.execute(
                    """
                        SELECT id
                        FROM auths
                        WHERE email = %s;
                    """,
                    [
                        other_person[
                            'email'
                        ]
                    ]
                ).fetchone() == None
