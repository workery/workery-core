pytest_plugins = [
    'tests.plugins.main',
    'tests.plugins.persons',
    'tests.plugins.roles',
    'tests.plugins.addresses',
]
