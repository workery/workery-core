include .env

cache=${HOME}/Projects/Workery/cache

cassandra:
	podman run --rm --replace \
	--name ${WORKERY_NAME}-cassandra \
	--publish 7000:7000 \
	--volume ${cache}/cassandra:/var/lib/cassandra \
	--detach cassandra:latest
clean:
	sudo rm -rRf ${cache}/cassandra/*
	sudo rm -rRf ${cache}/postgres/*
	sudo rm -rRf ${cache}/redis/*
	sudo rm -rRf ${cache}/rethinkdb/*
debug:
	ipython -i -m workery_core
pg:
	podman run --rm --replace \
		--name ${WORKERY_NAME}-postgres \
		--publish 5432:5432 \
		--env POSTGRES_PASSWORD=${WORKERY_KEY} \
		--env POSTGRES_USER=${WORKERY_NAME} \
		--volume ${cache}/postgres:/var/lib/postgresql \
		--detach postgres:alpine
pods:
	make pg
	make rethinkdb
	make redis
	python configure_databases.py
psql:
	psql ${WORKERY_SQL}
redis:
	podman run --rm --replace\
		--name ${WORKERY_NAME}-redis \
		--publish 6379:6379 \
		--volume ${cache}/redis:/data \
		--detach redis:alpine
rethinkdb:
	podman run --rm --replace\
		--name ${WORKERY_NAME}-rethinkdb \
		--publish 28015:28015 \
		--publish 8080:8080 \
		--volume ${cache}/rethinkdb:/data/rethinkdb_data \
		--detach rethinkdb:latest
tables:
	psql --file tables.psql ${WORKERY_SQL}
test:
	python -m pytest \
		--cov-report term-missing \
		--cov=workery_core \
		--verbosity=9 \
		--showlocals \
		-s

make all:
	make pods ; make test
make pip:
	python requirements.py
doc:
	cd docs ; mkdocs serve
