# Person

This class brings together all the others, thus centralizing all processes related to user administration.

Let's first create a user. To do this, you must provide a valid email, a standard telephone number and a password greater than or equal to 6 digits.

```python
from workery_core import Person

Person.auth.new(
    email='my_own@mail.com',
    phone='+5512345678901',
    password='123456'
)
>>> True
```

To log in to the system, it is necessary to obtain the registered ID. To do this, we can use the registered email:

```python
my_id = Person.auth.login(
    value='my_own@mail.com',
    password='123456'
)
my_id
>>> '7895893a-5714-41bb-b371-f3279fa27251'
```

... or the phone:

```python
my_id = Person.auth.login(
    value='+5512345678901',
    password='123456'
)
my_id
>>> '7895893a-5714-41bb-b371-f3279fa27251'
```

We do this so we can start the Person class:

```python
me = Person(
    identifier=my_id
)
```

Now, let's create an address:

```python
me.address.new(
    location='Rua das casas, sn',
    district='Beco do caixão',
    city='Piripiri',
    state='Solido',
    country='Samsara'
)
>>> 'ee8bafb1-4d27-4354-902d-bfdd2d458e93'
```

The returned value is the unique identifier of the registered address:

```python
me.address.all
>>> [{'city': 'Piripiri',
>>>  'country': 'Samsara',
>>>  'district': 'Beco do caixão',
>>>  'id': 'ee8bafb1-4d27-4354-902d-bfdd2d458e93',
>>>  'location': 'Rua das casas, sn',
>>>  'person': '7895893a-5714-41bb-b371-f3279fa27251',
>>>  'state': 'Solido'}]
```
















