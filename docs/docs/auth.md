# Authentication

Initially, you need to create a new user:

```python
from workery_core import Person

Person.auth.new(
    phone='+5512345678901',
    email='your_account@email.com',
    password='V3rY$7r0n6p45$w0rD4N07h1n6'
)
>>> True
```
Now you need to log in. You can do this using either your registered email or phone number. If you make a mistake, nothing will be returned. However, if everything goes well, your id registered in the database will be returned:


```python
error = Person.auth.login(
    value='+5512345678901',
    password='wrong_password'
)
print(error)
>>> None
my_uuid = Person.auth.login(
    value='+5512345678901',
    password='V3rY$7r0n6p45$w0rD4N07h1n6'
)
my_uuid
>>> 06191f29-b2cb-4669-a216-8bb1003209a8
```
This still doesn't do anything. You must instantiate the Person class:

```python
me = Person(
    identifier=my_uuid
)
me.auth.__dict__
>>> {'identifier': UUID('06191f29-b2cb-4669-a216-8bb1003209a8'),
>>> 'phone': '+5513992031899',
>>> 'email': 'eumesmo@exemplo.com',
>>> 'google': None,
>>> 'apple': None,
>>> 'create_in': datetime.datetime(2024, 2, 15, 18, 57, 27, 114511),
>>> 'enable_in': None,
>>> 'roles': []}
```
You may want to change your email:

```
me.auth.change_email(
    new_email='my_new@email.net'
)
>>> True
```
... or your phone number:

```
me.auth.change_phone(
    new_phone='+5510987654321'
)
>>> True
```

The same logic applies to modifying your password:

```
me.auth.change_password(
    new_password='wR0n6p4$5w0rD4N07h1n6'
)
>>> True
```

Note that your data has been changed:

```
me.auth.__dict__
>>> {'identifier': UUID('06191f29-b2cb-4669-a216-8bb1003209a8'),
>>> 'phone': '+5510987654321',
>>> 'email': 'my_new@email.net',
>>> 'google': None,
>>> 'apple': None,
>>> 'create_in': datetime.datetime(2024, 2, 15, 18, 57, 27, 114511),
>>> 'enable_in': None,
>>> 'roles': []}
```
