# Key

This class is responsible for creating and retrieving information on a Redis server.
The logic is simple: when logged into the system, you send your unique identifier to this class, it creates a key and sends your identifier to the server with the key that will be sent for later retrieval.
The next time you communicate with this class, you send the key that was previously passed to you. If this key exists in the database and the information contained in this key is the same as the one sent, the class deletes this key and creates another, where it stores its unique identifier again. If the key you sent does not exist or the user information is wrong, the class returns an exception.

```python
from workery_core import Key
my_uuid = 'f59b0c77-c7a2-467e-91f9-580048444aa9'
key = Key.new(my_uuid)
key
>>> '0udvPpxmtqJDvBey9xp4'
new_key = Key.recover(
    key=key,
    value=my_uuid
)
new_key
>>> 'LxOonVnZpIyRPsE2A3Bh'
from workery_core import SecurityError
try:
    Key.recover(
        key=new_key,
        value='other_value'
    )
except SecurityError as error:
    print(error.message)
>>> 'Error in key LxOonVnZpIyRPsE2A3Bh or value other_value.'
try:
    Key.recover(
        key=new_key,
        value=my_uuid
    )
except SecurityError as error:
    print(error.message)
>>> 'Key 0udvPpxmtqJDvBey9xp4 not found.'

```
