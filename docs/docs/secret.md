# Secret

This class deals with text encryption. It can receive a string or bytecodes and returns a string.

```python
from workery_core import Secret

text = 'Any text'
encrypted_text = Secret.encrypt_text(text)
encrypted_text
>>> 'gAAAAABl0Trbh8StmZxsK7uARTtuKCXnt3xQ7rYmLWo_fEKgtVnNtAPdjBiIj_KPYFJex88dLNPvjnPaMjtOiz3AYI77D3Ng1w=='
Secret.decrypt_text(encrypted_text)
>>> 'Any text'
bytecode_text = b'Any text'
encrypted_bytecode = Secret.encrypt_text(text)
encrypted_bytecode
>>> 'gAAAAABl0Tuvagf4oLmREtU3OceU_kBbPbuCtx3lUlyUpjmtQbj4VjoYvDhRjTumkKPhs12JMO7en2nikmhd7kJasj-fwKB3_Q=='
Secret.decrypt_text(encrypted_bytecode)
>>> 'Any text'
```
