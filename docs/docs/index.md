# Workery Core
Core of the Workery ecosystem.

## Topics

[Creating a person](person.md)

[Authentication](auth.md)

[Address](address.md)
