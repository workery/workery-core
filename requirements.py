from pathlib import Path
from tomllib import loads

values = loads(
    Path(
        Path.cwd() / 'pyproject.toml'
    ).read_text()
)['tool']['poetry']['dependencies']
values.pop('python')

packages = ''
for value in values:
    packages += f'{value}=={values[value]}\n'


Path(
    Path.cwd() / 'requirements.txt'
).write_text(packages)
