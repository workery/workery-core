redis==^5.0.1
rethinkdb==^2.4.10.post1
cassandra-driver==^3.29.0
psycopg==^3.1.18
jupyterlab==^4.1.0
argon2-cffi==^23.1.0
email-validator==^2.1.0.post1
firebird-driver==^1.10.1
